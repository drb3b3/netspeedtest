#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_SIZE 8192

int main(int argc, char *argv[])
{
	/* my super cool data to send */
	char data[BUFFER_SIZE] = "asd";

	struct sockaddr_in sa;
	int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd == -1) {
		perror("Couldn't create socket.");
		exit(EXIT_FAILURE);
	}

	memset(&sa, 0, sizeof(sa));

	sa.sin_family = AF_INET;
	sa.sin_port = htons(1553);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sockfd, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
		perror("Binding socket failed.");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	if (listen(sockfd, 10) == -1) {
		perror("Listen failed");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	while (1) {
		int confd = accept(sockfd, NULL, NULL);

		if (confd < 0) {
			perror("Accept failed.");
			close(sockfd);
			exit(EXIT_FAILURE);
		}

		/* perform read and write stuff */
		unsigned send_count = 0;
		unsigned i;
		if (recv(confd, &send_count, sizeof(send_count), 0) == -1) {
			perror("recv() failed: don't know how many packets to send.");
		}

		printf("Sending %d dick times.\n", send_count);
		for (i = 0; i < send_count; ++i) {
			if (send(confd, data, sizeof(data), 0) == -1) {
				//perror("send() failed.");
			}
		}
				printf("Heyyoooo\n");


		if (shutdown(confd, SHUT_RDWR) == -1) {
			perror("Shutdown failed.");
			close(confd);
			close(sockfd);
			exit(EXIT_FAILURE);
		}

		close(confd);
	}

	close(sockfd);

	return EXIT_SUCCESS;
}
