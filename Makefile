CC=gcc
RM=rm -f
CFLAGS=
LIBS=
DEPS=
SRV_OBJ=server.o
CLN_OBJ=client.o

netspeedtest: server client
	
server: $(SRV_OBJ)
	$(CC) $(CFLAGS) -o server $(SRV_OBJ)

client: $(CLN_OBJ)
	$(CC) $(CFLAGS) -o client $(CLN_OBJ)

server.o: server.c
	$(CC) $(CFLAGS) -c server.c

clean:
	$(RM) $(SRV_OBJ) server $(CLN_OBJ) client
