#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define BUFFER_SIZE 8192

typedef struct {
	struct timespec start;
	struct timespec stop;
} mytimer_t;

void start(mytimer_t *timer);
void stop(mytimer_t *timer);
double secs_elapsed(mytimer_t *timer);

int main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("Usage: ./client <server>\n");
		exit(EXIT_FAILURE);
	}

	char buffer[BUFFER_SIZE];
	struct sockaddr_in sa;
	int res;
	int sockfd;

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd == -1) {
		perror("Socket() failed.");
		exit(EXIT_FAILURE);
	}

	memset(&sa, 0, sizeof(sa));

	sa.sin_family = AF_INET;
	sa.sin_port = htons(1553);
	res = inet_pton(AF_INET, argv[1], &sa.sin_addr);

	if (connect(sockfd, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
		perror("Connect() failed.");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	/* read and write shit */
	unsigned i;
	unsigned recv_count = 1000000;
	mytimer_t timer;
	start(&timer);
	send(sockfd, &recv_count, sizeof(recv_count), 0);
	for (i = 0; i < recv_count; ++i) {
		recv(sockfd, buffer, BUFFER_SIZE, 0);
		printf("Recv: %s\n", buffer);
	}
	stop(&timer);
	printf("Timer %f\n", secs_elapsed(&timer));
	shutdown(sockfd, SHUT_RDWR);
	close(sockfd);

	return EXIT_SUCCESS;
}

void start(mytimer_t *timer)
{
	if (timer != NULL)
		clock_gettime(CLOCK_MONOTONIC, &(timer->start));
}

void stop(mytimer_t *timer)
{
	if (timer != NULL)
		clock_gettime(CLOCK_MONOTONIC, &(timer->stop));
}

double secs_elapsed(mytimer_t *timer)
{
	return (timer->stop.tv_sec - timer->start.tv_sec) * 1e6 + (timer->stop.tv_nsec - timer->start.tv_nsec);
}
